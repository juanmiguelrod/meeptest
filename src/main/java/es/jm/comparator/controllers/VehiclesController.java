package es.jm.comparator.controllers;

import es.jm.comparator.json.model.Vehicle;
import es.jm.comparator.services.Comparator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class VehiclesController {

    @GetMapping("/getVehicles")
    public HashMap<String, List<Vehicle>> getVehicles(){
        return Comparator.getInstance().getResult();
    }
}
