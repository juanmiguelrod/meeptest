package es.jm.comparator.schedulers;

import es.jm.comparator.services.Comparator;
import org.quartz.DisallowConcurrentExecution;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class VehiclesJob  {


    @Scheduled( cron="*/30 * * * * ?")
    public void jobDetail() {
        System.out.println("INICIADO ******** "+new Date());
        Comparator.getInstance().read();
    }


}
