package es.jm.comparator.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import es.jm.comparator.json.model.Vehicle;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Comparator {


    private static Comparator mInstance;

    private List<Vehicle> old = new ArrayList<>();
    private List<Vehicle> same = new ArrayList<>();
    private List<Vehicle> news = new ArrayList<>();
    private List<Vehicle> removed = new ArrayList<>();

    final static String URL = "https://apidev.meep.me/tripplan/api/v1/routers/lisboa/resources?lowerLeftLatLon=38.711046,-9.160096&upperRightLatLon=38.739429," +
            "-9.137115&companyZoneIds=545,467,473";

    private Comparator() {
    }

    public static Comparator getInstance() {
        if (mInstance == null) {
            mInstance = new Comparator();
        }
        return mInstance;
    }


    public HashMap<String,List<Vehicle>> getResult(){
        HashMap returnObject= new HashMap();
        returnObject.put("news", news);
        returnObject.put("removed", removed);
        returnObject.put("same",same);
        return returnObject;
    }

    public void read() {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String answer = restTemplate.getForObject(URL, String.class);

        Type listType = new TypeToken<List<Vehicle>>() {
        }.getType();
        List<Vehicle> vehicles = new Gson().fromJson(answer, listType);
        LocalDate init= LocalDate.now();

        news = new ArrayList<>();
        same= new ArrayList<>();

        for (Vehicle vehicle : vehicles) {
            if (existOnList(vehicle, old)){
                same.add(vehicle);
            }
            else{
                news.add(vehicle);
            }
        }

        removed = new ArrayList<>(old);
        old= new ArrayList<>();
        old.addAll(news);
        old.addAll(same);


        System.out.println("Tiempo de proceso: "+Period.between(init, LocalDate.now()).getDays());
        System.out.println("news:" + news.size());
        System.out.println("removed:" + removed.size());
        System.out.println("static:" + same.size());
    }

    private  boolean existOnList(Vehicle vehicle, List<Vehicle> listVehicles) {
        for (Vehicle vehicleTmp : listVehicles) {
            if (vehicleTmp.getId().equals(vehicle.getId())){
                listVehicles.remove(vehicleTmp);//eliminamos para acelerar el proceso
                return true;
            }
        }
        return false;
    }


}
