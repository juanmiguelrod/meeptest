# MeepTest
requiere mvn y java 11

1- Arrancar.

mvn spring-boot:run

2- acceder desde navegador:

http://localhost:8090/getVehicles

Devuelve json en formato:
{"news":[],
"same":[],
"removed":[],
}
Siendo news los vehiculos nuevos, removed los que desaparecieron y same los que no se han movido.

--------------------------------------------------------
¿Cómo de escalable es tu solución propuesta?

Tal y como está poco, solo filtra para esa cuadrícula y esos medios de transporte, poco práctico, pero si lo cambiamos para que filtre
por una área mas grande y mas tipos de medios de transporte se podría tener varias máquinas para que guarden mas rápido.

Se podría poner varios servicios como este escaneando diferentes áreas del mapa y grabando en bbdd los
vehiculos disponibles en cada punto. La parte front consumiría otro servicio que tiraría de bbdd.

Habría que analizar si es mas rápido escanear directamente donte están todos los vehiculos y actualizar su posición y disponibilidad
en lugar de buscar por áreas. Es decir, que el proveedor nos avise de cuando un vehiculo está disponible o deja de estarlo y donde.

El algoritmo de filtrado que he usado no es el más rápido, se podría optimizar.

¿Que problemas a futuro podría presentar? Si has detectado alguno,

- Si haces la petición y pilla a mitad del proceso, se muestran datos parciales.
- Hay 30 segundos en los que un vehículo puede ser cogido y la app muestra que está disponible.

¿Qué alternativa/s propones para solventar dichos problemas?

Llevo 3 horas con la prueba... y este apartado daría para varios días :)